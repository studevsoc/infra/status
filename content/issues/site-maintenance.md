---
title: "Website Maintenance"
date: 2021-08-25T03:55:00+05:30
informational: true
severity: notice
affected: 
    - SDS Main Website
section: issue
resolvedWhen: 2021-08-25T04:00:00
resolved: true
---

SDS Homepage will be down for some time as we do secuirity updates and restart the server.

### Update
Server Maintenance complete.
